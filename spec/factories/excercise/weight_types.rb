FactoryBot.define do
  factory :excercise_weight_type, class: 'Excercise::WeightType' do
    name_en { "MyString" }
    name_ru { "MyString" }
  end
end
