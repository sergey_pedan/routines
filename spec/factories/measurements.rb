FactoryBot.define do
  factory :measurement do
    value { 1.5 }
    lab { nil }
  end
end
