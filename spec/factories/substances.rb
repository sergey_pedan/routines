FactoryBot.define do
  factory :substance do
    name { "MyString" }
    daily_dosage { "MyString" }
  end
end
