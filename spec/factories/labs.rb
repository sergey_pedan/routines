FactoryBot.define do
  factory :lab do
    name { "MyString" }
    city { nil }
    street_address { "MyString" }
  end
end
