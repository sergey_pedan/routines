class AddRepetitionsCountAvgToExcercises < ActiveRecord::Migration[6.1]
  def change
    add_column :excercises, :repetitions_count_avg,   :decimal, default: 0, null: false
    add_column :excercises, :resistance_duration_avg, :decimal, default: 0, null: false
    add_column :excercises, :weight_avg,              :decimal, default: 0, null: false
  end
end
