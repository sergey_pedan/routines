# frozen_string_literal: true

class WeightBundle < ApplicationRecord
end

# == Schema Information
# Schema version: 20200414044540
#
# Table name: weight_bundles
#
#  id     :bigint           not null, primary key
#  name   :string           not null
#  weight :decimal(, )      not null
#
